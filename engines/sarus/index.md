---
title: "Sarus"
weight: 2
---
Sarus is more than a runtime. It also takes care of the lifecycle of images, snapshots and containers. 

Let's pull an image to the users registry first.

```bash
sarus pull alpine:3.15
sarus images
```

Output:

```{ bash .no-copy}
$ sarus pull alpine:3.15
# image            : index.docker.io/library/alpine:3.15
# cache directory  : "/home/alice/.sarus/cache"
# temp directory   : "/tmp"
# images directory : "/home/alice/.sarus/images"
> save image layers ...
> pulling        : sha256:59bf1c3509f33515622619af21ed55bbe26d24913cedbca106468a5fb37a50c3
> completed      : sha256:59bf1c3509f33515622619af21ed55bbe26d24913cedbca106468a5fb37a50c3
> expanding image layers ...
> extracting     : "/home/alice/.sarus/cache/sha256:59bf1c3509f33515622619af21ed55bbe26d24913cedbca106468a5fb37a50c3.tar"
> make squashfs image: "/home/alice/.sarus/images/index.docker.io/library/alpine/3.15.squashfs"
$ sarus images
REPOSITORY   TAG          DIGEST         CREATED               SIZE         SERVER
alpine       3.15         08410c48b865   2022-03-16T11:17:54   2.61MB       index.docker.io
```

### Run

A simple run looks like this:

```bash
sarus run alpine:3.15 cat /etc/os-release
```

Output:

```bash
$ sarus run alpine:3.15 cat /etc/os-release
NAME="Alpine Linux"
ID=alpine
VERSION_ID=3.15.0
PRETTY_NAME="Alpine Linux v3.15"
HOME_URL="https://alpinelinux.org/"
BUG_REPORT_URL="https://bugs.alpinelinux.org/"
```
