---
title: "Introduction"
weight: 1
---
This chapter introduces all components needed within the workshop. Even if you know the projects and tools already we recommend you at least have a look. :)
