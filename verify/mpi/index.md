---
title: "MPI"
weight: 2
---
To verify that `mpi` works as expected, let us compile a small hello-world example.

```bash
cat > hello.c << \EOF
#include <mpi.h>
#include <stdio.h>

int main(int argc, char** argv) {
    // Initialize the MPI environment
    MPI_Init(NULL, NULL);

    // Get the number of processes
    int world_size;
    MPI_Comm_size(MPI_COMM_WORLD, &world_size);

    // Get the rank of the process
    int world_rank;
    MPI_Comm_rank(MPI_COMM_WORLD, &world_rank);

    // Get the name of the processor
    char processor_name[MPI_MAX_PROCESSOR_NAME];
    int name_len;
    MPI_Get_processor_name(processor_name, &name_len);

    // Print off a hello world message
    printf("Hello world from processor %s, rank %d out of %d processors\n",
           processor_name, world_rank, world_size);

    // Finalize the MPI environment.
    MPI_Finalize();
}
EOF
```

Now, let us load openmpi, compile and testrun the script.

```bash
spack load openmpi
mpicc -o hello hello.c
mpirun -n 2 $(pwd)/hello 
```

The output.

```bash
$ mpirun -n 2 $(pwd)/hello
Hello world from processor headnode, rank 0 out of 2 processors
Hello world from processor headnode, rank 1 out of 2 processors
```
