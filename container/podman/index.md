---
title: "Podman"
weight: 2
---
Podman was created to replace Docker with a daemonless setup (more details [here](https://podman.io/whatis.html)).

### Image Pull

First we download the OCI image [alpine:3.15](https://hub.docker.com/_/alpine) from DockerHub. This uses layers and metadata to optimize the download and reuse layers already present.

```bash
podman pull alpine:3.15
```

The result is one layer being downloaded.

```bash
$ podman pull alpine:3.15
Resolved "alpine" as an alias (/etc/containers/registries.conf.d/000-shortnames.conf)
Trying to pull docker.io/library/alpine:3.15...
Getting image source signatures
Copying blob 3d2430473443 done
Copying config e9adb5357e done
Writing manifest to image destination
Storing signatures
e9adb5357e84d853cc3eb08cd4d3f9bd6cebdb8a67f0415cc884be7b0202416d
```

### Snapshot 
Next, we snapshot the downloaded image into a container filesystem.

```bash
podman create -ti --rm --name alp315 alpine:3.15 cat /etc/os-release
podman container ls -a
```

We created a container snapshot and configuration ready to be started.

```bash
$ podman create -ti --rm --name alp315 alpine:3.15 cat /etc/os-release
8057a8eeb2bc096f33bf5946ce68df8a9e3e7f2b241bbef173842c8c29ff4a5b
$ podman container ls -a
CONTAINER ID   IMAGE         COMMAND                 CREATED          STATUS    PORTS     NAMES
8057a8eeb2bc   alpine:3.15   "cat /etc/os-release"   53 seconds ago   Created             alp315
```

### Run

```bash
podman start -a alp315
```

`podman start` actually spawns the process, attached to stdout (`-a`), and removes the container afterwards (b/c we used `--rm` when creating it).

```bash
$ podman start -a alp315
NAME="Alpine Linux"
ID=alpine
VERSION_ID=3.15.0
PRETTY_NAME="Alpine Linux v3.15"
HOME_URL="https://alpinelinux.org/"
BUG_REPORT_URL="https://bugs.alpinelinux.org/"
```

## One Shot

We can combine all three commands into one: `podman run` with a different image version.

```bash
podman run -ti --rm alpine:3.14 cat /etc/os-release
```

This command will download the iamge, create the snapshot and config, and run the container afterward.

```bash
$ podman run -ti --rm alpine:3.14 cat /etc/os-release
Resolved "alpine" as an alias (/etc/containers/registries.conf.d/000-shortnames.conf)
Trying to pull docker.io/library/alpine:3.14...
Getting image source signatures
Copying blob 36ccefbf3d8a done
Copying config a33ac4f106 done
Writing manifest to image destination
Storing signatures
NAME="Alpine Linux"
ID=alpine
VERSION_ID=3.14.4
PRETTY_NAME="Alpine Linux v3.14"
HOME_URL="https://alpinelinux.org/"
BUG_REPORT_URL="https://bugs.alpinelinux.org/"
```
